package domain;

/**
 * Created by galik on 31.01.2016.
 */
public enum SerieType {
    Line, Point, LinePoint, Bar, Area
}
