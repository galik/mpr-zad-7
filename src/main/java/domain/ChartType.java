package domain;

/**
 * Created by galik on 31.01.2016.
 */
public enum ChartType {
    Line, Point, LinePoint, Bar, StackedBar, Area, Pie
}
