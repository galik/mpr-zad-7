package domain;

/**
 * Created by galik on 31.01.2016.
 */
public class Point {

    private double x;
    private double y;

    public Point(int a, int b) {
        x = a;
        y = b;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
