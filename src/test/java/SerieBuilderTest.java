import domain.ChartSerie;
import domain.Point;
import domain.SerieType;
import org.junit.Before;
import org.junit.Test;
import services.SerieBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by galik on 31.01.2016.
 */
public class SerieBuilderTest {

    private SerieBuilder serieBuilder;

    @Before
    public void before() {
        serieBuilder = new SerieBuilder();
    }

    @Test
    public void addPoint_WithPoint_ContainsExactly() {
        Point point = new Point(1, 1);
        ChartSerie serie = serieBuilder.addPoint(point).build();
        assertThat(serie.getPoints()).isNotNull().containsExactly(point);
    }

    @Test
    public void addPoint_WithoutPoint_ContainsNull() {
        ChartSerie serie = serieBuilder.addPoint(null).build();
        assertThat(serie.getPoints()).containsNull();
    }

    @Test
    public void addLabel_WithLabel_ContainsIgnoringCase() {
        String label = "label";
        ChartSerie serie = serieBuilder.addLabel(label).build();
        assertThat(serie.getLabel()).containsIgnoringCase("LaBeL");
    }

    @Test
    public void addLabel_WithLabel_DoesNotMatch() {
        String label = "blabla";
        ChartSerie serie = serieBuilder.addLabel(label).build();
        assertThat(serie.getLabel()).doesNotMatch("/label/");
    }

    @Test
    public void addPoints_WithPoints_ContainsAll() {
        Point point1 = new Point(1,1);
        Point point2 = new Point(2,2);
        Point point3 = new Point(3,3);
        List<Point> points = Arrays.asList(point1, point2, point3);
        ChartSerie serie = serieBuilder.addPoints(points).build();
        assertThat(serie.getPoints()).isNotNull().containsAll(points);
    }

    @Test
    public void addPoints_WithEmptyArray_ContainsNull() {
        List<Point> points = new ArrayList<Point>();
        points.add(null);
        ChartSerie serie = serieBuilder.addPoints(points).build();
        assertThat(serie.getPoints()).containsNull();
    }

    @Test
    public void setType_WithType_isEqualTo() {
        SerieType type = SerieType.Area;
        ChartSerie serie = serieBuilder.setType(type).build();
        assertThat(serie.getSerieType()).isEqualTo(type);
    }

    @Test
    public void setType_WithoutType_isNull() {
        ChartSerie serie = serieBuilder.setType(null).build();
        assertThat(serie.getSerieType()).isNull();
    }
}
