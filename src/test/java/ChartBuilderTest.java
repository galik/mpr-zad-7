import domain.*;
import services.ChartBuilder;

import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by galik on 31.01.2016.
 */
public class ChartBuilderTest {

    private ChartBuilder chartBuilder;

    @Before
    public void before() {
        chartBuilder = new ChartBuilder();
    }

    @Test
    public void addSerie_WithSerie_IsNotNull() {
        String label1 = "label1";
        Point point1 = new Point(1,1);
        Point point2 = new Point(2,2);
        SerieType serieType = SerieType.Line;
        List<Point> points = Arrays.asList(point1, point2);

        ChartSerie chartSerie = new ChartSerie();
        chartSerie.setLabel(label1);
        chartSerie.setPoints(points);
        chartSerie.setSerieType(serieType);

        ChartSettings chartSettings = chartBuilder.addSerie(chartSerie).build();

        assertThat(chartSettings.getSeries().get(0)).isNotNull().isEqualTo(chartSerie);
    }

    @Test
    public void addSerie_WithoutSerie_IsNull() {
        ChartSettings chartSettings = chartBuilder.addSerie(null).build();
        assertThat(chartSettings.getSeries().get(0)).isNull();
    }

    @Test
    public void withSeries_WithSeries_ContainsSerie() {
        String label1 = "label1";
        Point point1 = new Point(1,1);
        Point point2 = new Point(2,2);
        SerieType serieType = SerieType.Line;
        List<Point> points = Arrays.asList(point1, point2);

        ChartSerie chartSerie = new ChartSerie();
        chartSerie.setLabel(label1);
        chartSerie.setPoints(points);
        chartSerie.setSerieType(serieType);
        List<ChartSerie> chartSeries = new ArrayList<ChartSerie>();
        chartSeries.add(chartSerie);

        ChartSettings chartSettings = chartBuilder.withSeries(chartSeries).build();

        assertThat(chartSettings.getSeries()).contains(chartSerie);
    }

    @Test
    public void withSeries_WithoutSerie_ContainsNull() {
        ChartSettings chartSettings = chartBuilder.addSerie(null).build();
        assertThat(chartSettings.getSeries()).containsNull();
    }

    @Test
    public void withTitle_WithTitle_IsNotNull() {
        String title = "title";
        ChartSettings chart = chartBuilder.withTitle(title).build();
        assertThat(chart.getTitle()).isNotNull().isEqualTo(title);
    }

    @Test
    public void withTitle_WithoutTitle_IsNull() {
        ChartSettings chart = chartBuilder.withTitle(null).build();
        assertThat(chart.getTitle()).isNull();
    }

    @Test
    public void withSubtitle_WithSubtitle_IsNotNull() {
        String subtitle = "subtitle";
        ChartSettings chart = chartBuilder.withSubtitle(subtitle).build();
        assertThat(chart.getSubtitle()).isNotNull().isEqualTo(subtitle);
    }

    @Test
    public void withSubtitle_WithoutSubtitle_IsNull() {
        ChartSettings chart = chartBuilder.withSubtitle(null).build();
        assertThat(chart.getSubtitle()).isNull();
    }

    @Test
    public void isHaveLegend_WithLegend_IsTrue() {
        ChartSettings chart = chartBuilder.withLegend().build();
        assertThat(chart.isHaveLegend()).isTrue();
    }

    @Test
    public void isHaveLegend_WithoutLegend_IsFalse() {
        ChartSettings chart = chartBuilder.build();
        assertThat(chart.isHaveLegend()).isFalse();
    }

    @Test
    public void withType_Point_IsEqualTo() {
        ChartType type = ChartType.Point;
        ChartSettings chart = chartBuilder.withType(type).build();
        assertThat(chart.getChartType()).isNotNull().isEqualTo(type);
    }

    @Test
    public void withType_Null_IsNull() {
        ChartSettings chart = chartBuilder.withType(null).build();
        assertThat(chart.getChartType()).isNull();
    }
}
